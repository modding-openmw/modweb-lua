--
-- Turn <a> and <link> hrefs and <img> and <script> srces into versions that
-- will play nicely with how Gitlab's (and maybe also others') static site
-- hosting works.
--
-- This allows the files to default to a path that's friendly to local usage,
-- and then in production they will use the currect paths.
--
-- To use, add the following to your soupault config file:
--
-- [widgets.prod-mode]
--   widget = "prod-mode"
--   project = "your-mod-repo-slug-here"
--
-- And include this script in your project's plugins directory. Replace
-- `your-mod-repo-slug-here` with the slug of your mod's Gitlab repo.
--
-- Minimum soupault version: 4.3.0
-- Author: Hristos N. Triantafillou
-- License: MIT
--

Plugin.require_version("4.3.0")

local things = HTML.select_all_of(page, {"a", "img", "link", "script", "source"})
local project = config["project"]

local n = 1
local c = size(things)
while (n <= c) do
    local thing = things[n]

    local src = HTML.get_attribute(thing, "src")
    if src ~= nil then
        if String.starts_with(src, "/") then
            HTML.set_attribute(thing, "src", "/" .. project .. src)
        elseif String.starts_with(src, ".") then
            HTML.set_attribute(thing, "src", "/" .. project .. Regex.replace(src, "\.", ""))
        end
    end

    local href = HTML.get_attribute(thing, "href")
    if href ~= nil then
        if String.starts_with(href, "/") then
            HTML.set_attribute(thing, "href", "/" .. project .. href)
        end
    end

    local srcset = HTML.get_attribute(thing, "srcset")
    if srcset ~= nil then
        if String.starts_with(srcset, "/") then
            HTML.set_attribute(thing, "srcset", "/" .. project .. srcset)
        end
    end
n = n + 1
end
