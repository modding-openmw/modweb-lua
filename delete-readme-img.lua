--
-- Delete an image with the given src value
--
-- Minimum soupault version: 4.3.0
-- Author: Hristos N. Triantafillou
-- License: MIT
--

Plugin.require_version("4.3.0")

local things = HTML.select_all_of(page, {"img"})
local src_name = config["src_name"]
local n = 1
local c = size(things)

while (n <= c) do
    local thing = things[n]
    local src = HTML.get_attribute(thing, "src")
    if src ~= nil then
        if src == src_name then
            HTML.delete_element(thing)
        end
    end
    n = n + 1
end
